const express = require(`express`);

const app = express();
const PORT = 8000;
const cors = require(`cors`);
app.use(cors());

const convertRoute = require(`./routes/route_convert`);

app.use('/convert', convertRoute);

app.listen(PORT, () => {
  console.log(`Server of School's Library runs on port
    ${PORT}`);
});
