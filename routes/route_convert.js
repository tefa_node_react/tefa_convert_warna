const express = require('express')
const app = express()
app.use(express.json())

const controlerConvert = require("../controller/controller_convert")

app.get("/:hex", controlerConvert.konvert);
    
module.exports = app